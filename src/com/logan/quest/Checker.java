package com.logan.quest;

import org.rspeer.runetek.api.Varps;
import org.rspeer.runetek.api.component.Interfaces;
import org.rspeer.script.task.Task;
import org.rspeer.ui.Log;


public class Checker extends Task {

    @Override
    public boolean validate() {
        return (Varps.get(144) == 100) && Varps.get(107) == 5 && Varps.get(179) == 21;
    }

    @Override
    public int execute() {
        Log.info("All quests are completed. Stopping the script and logging out.");
        Interfaces.getComponent(182, 8).interact("Logout");
        return -1;
    }
}


