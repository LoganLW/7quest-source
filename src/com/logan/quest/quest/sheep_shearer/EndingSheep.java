package com.logan.quest.quest.sheep_shearer;

import com.logan.quest.Quest;
import org.rspeer.runetek.adapter.scene.Npc;
import org.rspeer.runetek.adapter.scene.Player;
import org.rspeer.runetek.api.commons.Time;
import org.rspeer.runetek.api.component.Dialog;
import org.rspeer.runetek.api.movement.Movement;
import org.rspeer.runetek.api.movement.position.Position;
import org.rspeer.runetek.api.scene.Npcs;
import org.rspeer.runetek.api.scene.Players;
import org.rspeer.script.task.Task;


public class EndingSheep extends Task {

    private final Player local = Players.getLocal();

    @Override
    public boolean validate() {
        return Quest.SHEEP_SHEARER.getVarpValue() == 20;
    }

    @Override
    public int execute() {
        if (Movement.getRunEnergy() > 20 && !Movement.isRunEnabled()) { //Turn on run if it's off with over 20 energy
            Movement.toggleRun(true);

        }
        if (Dialog.canContinue()) {
            if (Dialog.processContinue()) {
                Time.sleep(500);
            }
        } else {
            final Npc Fred = Npcs.getNearest(732);
            if (Fred != null) {
                Movement.buildPath(Fred).walk();
                Time.sleepUntil((() -> Fred.distance(local) < 2), 300, 400);
                Fred.interact("Talk-to");
            } else {
                Movement.buildPath(new Position(3189, 3275, 0)).walk();
            }
        }

        return 300;
    }
}


