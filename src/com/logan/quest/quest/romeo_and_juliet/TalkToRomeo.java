package com.logan.quest.quest.romeo_and_juliet;

import com.logan.quest.Quest;
import org.rspeer.runetek.adapter.scene.Npc;
import org.rspeer.runetek.adapter.scene.Player;
import org.rspeer.runetek.adapter.scene.SceneObject;
import org.rspeer.runetek.api.commons.Time;
import org.rspeer.runetek.api.component.Dialog;
import org.rspeer.runetek.api.movement.Movement;
import org.rspeer.runetek.api.movement.position.Position;
import org.rspeer.runetek.api.scene.Npcs;
import org.rspeer.runetek.api.scene.Players;
import org.rspeer.runetek.api.scene.SceneObjects;
import org.rspeer.script.task.Task;

public class TalkToRomeo extends Task {

    @Override
    public boolean validate() {
        return Quest.ROMEO_AND_JULIET.getVarpValue() == 20;
    }

    @Override
    public int execute() {
        SceneObject sTAIRCASE = SceneObjects.getNearest(11799);
        Player local = Players.getLocal();
        final Npc Romeo = Npcs.getNearest(5037);
        if (Movement.getRunEnergy() > 20 && !Movement.isRunEnabled()) { //Turn on run if it's off with over 20 energy
            Movement.toggleRun(true);
        }
        if(sTAIRCASE != null){
            if(sTAIRCASE.distance(local) < 3){
                sTAIRCASE.interact("Climb-down");
            }
            else{
                Movement.buildPath(new Position(3155, 3435, 1 )).walk();
            }
        }
        if(Romeo != null){
            if (Dialog.canContinue()) {
                if (Dialog.processContinue()) {
                    Time.sleep(1000);
                }
            }
            else{
                Movement.buildPath(Romeo).walk();
                Time.sleepUntil((() -> Romeo.distance(local) < 5), 300, 1500);
                Npcs.getNearest(5037).interact("Talk-to");
            }

        }
        else{
            if(local.getFloorLevel() != 1){
                Movement.buildPath(new Position(3213, 3428, 0)).walk();
            }
        }
        return 300;
    }
}