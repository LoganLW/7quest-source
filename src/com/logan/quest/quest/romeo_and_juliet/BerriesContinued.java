package com.logan.quest.quest.romeo_and_juliet;

import com.logan.quest.Quest;
import org.rspeer.runetek.adapter.scene.Npc;
import org.rspeer.runetek.adapter.scene.Player;
import org.rspeer.runetek.adapter.scene.SceneObject;
import org.rspeer.runetek.api.commons.Time;
import org.rspeer.runetek.api.component.Dialog;
import org.rspeer.runetek.api.component.Interfaces;
import org.rspeer.runetek.api.component.tab.Inventory;
import org.rspeer.runetek.api.movement.Movement;
import org.rspeer.runetek.api.movement.position.Position;
import org.rspeer.runetek.api.scene.Npcs;
import org.rspeer.runetek.api.scene.Players;
import org.rspeer.runetek.api.scene.SceneObjects;
import org.rspeer.script.task.Task;
import org.rspeer.ui.Log;

import java.util.function.Predicate;

public class BerriesContinued extends Task {
    private static final Predicate<String> TALK_ABOUT_PREDICATE = o -> o.contains("Talk about something");
    private static final Predicate<String> TALK_ABOUT_ROMEO_PREDICATE = o -> o.contains("Talk about Romeo");

    @Override
    public boolean validate() {
        return Quest.ROMEO_AND_JULIET.getVarpValue() == 50;
    }

    @Override
    public int execute() {
        Player local = Players.getLocal();
        final Npc Apothecary = Npcs.getNearest(5036);
        SceneObject Bush = SceneObjects.getNearest("Cadava Bush");
        if (Movement.getRunEnergy() > 20 && !Movement.isRunEnabled()) { //Turn on run if it's off with over 20 energy
            Movement.toggleRun(true);

        }
        if (Inventory.contains(756)) {
            final Npc Juliet = Npcs.getNearest(5035);
            if (local.getFloorLevel() == 1) {
                if (Dialog.canContinue()) {
                    if (Dialog.processContinue()) {
                        Time.sleepUntil(Dialog::canContinue, 300, 4000);
                    }
                } else {
                    Log.info(local.getTargetIndex());
                    if (Juliet != null) {
                        Movement.buildPath(Juliet).walk();
                        Time.sleepUntil((() -> Juliet.distance(local) < 2), 300, 4000);
                        Npcs.getNearest(5035).interact("Talk-to");
                    }
                }
            } else {
                SceneObject sTAIRCASE = SceneObjects.getNearest(11797);
                if (sTAIRCASE != null && sTAIRCASE.distance(local) < 10) {
                    sTAIRCASE.interact("Climb-up");
                    return 300;
                } else {
                    Movement.buildPath(new Position(3161, 3434)).walk();
                }

            }
            return 300;
        }
        if (!Inventory.contains(753) && !Inventory.contains(756)) {
            if (Bush != null && Bush.getId() != 23627) {
                Bush.interact("Pick-from");
            } else {
                Movement.buildPath(new Position(3271, 3369, 0)).walk();
            }
        } else {
            int canyouLength = Interfaces.filterByText(TALK_ABOUT_PREDICATE).length;
            int romeoLength = Interfaces.filterByText(TALK_ABOUT_ROMEO_PREDICATE).length;
            if (Apothecary != null) {
                if (Dialog.canContinue()) {
                    if (Dialog.processContinue()) {
                        Time.sleep(1000);
                    }
                } else {
                    if (canyouLength > 0) {
                        if (Dialog.process(1)) {
                            Time.sleep(1000);
                        }
                    }
                    if (romeoLength > 0) {
                        if (Dialog.process(0)) {
                            Time.sleep(1000);
                        }
                    } else {
                        if (canyouLength == 0 && romeoLength == 0 && !local.isMoving() && local.getAnimation() == -1) {
                            Apothecary.interact("Talk-to");
                        }

                    }
                }
            } else {
                if (local.getFloorLevel() != 1) {
                    Movement.buildPath(new Position(3195, 3403, 0)).walk();
                }
            }
        }
        return 1000;
    }
}

