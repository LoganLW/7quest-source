package com.logan.quest.quest.restless_ghost;

import com.logan.quest.Quest;
import org.rspeer.runetek.adapter.scene.Npc;
import org.rspeer.runetek.adapter.scene.Player;
import org.rspeer.runetek.api.commons.Time;
import org.rspeer.runetek.api.component.Dialog;
import org.rspeer.runetek.api.component.Interfaces;
import org.rspeer.runetek.api.movement.Movement;
import org.rspeer.runetek.api.movement.position.Position;
import org.rspeer.runetek.api.scene.Npcs;
import org.rspeer.runetek.api.scene.Players;
import org.rspeer.script.task.Task;

import java.util.function.Predicate;


public class StartRestlessGhost extends Task {
    private static final Predicate<String> IM_LOOKING_FOR_PREDICATE = o -> o.contains("looking for a quest");
    private static final Predicate<String> OKAY_LET_ME_PREDICATE = o -> o.contains("Ok, let me help");

    private final Player local = Players.getLocal();

    @Override
    public boolean validate() {
        return Quest.THE_RESTLESS_GHOST.getVarpValue() == 0;
    }

    @Override
    public int execute() {
        if (Movement.getRunEnergy() > 20 && !Movement.isRunEnabled()) { //Turn on run if it's off with over 20 energy
            Movement.toggleRun(true);

        }
        if (Dialog.canContinue()) {
            if (Dialog.processContinue()) {
                Time.sleep(1000);
            }
        } else {
            int lookingforLength = Interfaces.filterByText(IM_LOOKING_FOR_PREDICATE).length;
            int letmehelpLength = Interfaces.filterByText(OKAY_LET_ME_PREDICATE).length;
            if (lookingforLength > 0) {
                if (Dialog.process(2)) {
                    Time.sleep(1000);
                }
            } else if (letmehelpLength > 0) {
                if (Dialog.process(0)) {
                    Time.sleep(1000);
                }
            } else {
                final Npc Father_Aereck = Npcs.getNearest(921);
                if (lookingforLength == 0) {
                    if (Father_Aereck != null) {
                        Movement.buildPath(Father_Aereck).walk();
                        Time.sleepUntil((() -> Father_Aereck.distance(local) < 2), 300, 400);
                        Father_Aereck.interact("Talk-to");
                    } else {
                        Movement.buildPath(new Position(3243, 3210, 0)).walk();
                        return 300;
                    }
                }
            }
        }
        return 300;
    }

}


