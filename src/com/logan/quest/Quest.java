package com.logan.quest;

import org.rspeer.runetek.api.Game;
import org.rspeer.runetek.api.Varps;

public enum Quest {

    ROMEO_AND_JULIET(144),
    THE_RESTLESS_GHOST(107),
    SHEEP_SHEARER(179);

    private final int varpId;

    Quest(int varpId) {
        this.varpId = varpId;
    }

    public int getVarpId() {
        return varpId;
    }

    public int getVarpValue() {
        return Game.isLoggedIn() ? Varps.get(varpId) : -1;
    }

    @Override
    public String toString() {
        String fixed = name().toLowerCase().replace("_", " ");
        return fixed.substring(0, 1).toUpperCase().concat(fixed.substring(1));
    }
}
